import { Component } from '@angular/core';
import { FormControl, FormGroup, FormsModule, NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { ITask } from '../../models/task';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { DATE_FORMATS } from '../../models/date-formats';
import { TasksService } from '../../services/tasks.service';
import { Subscription } from 'rxjs';

const ELEMENT_TASK: ITask[] = [
  { taskName: '', executors: [], start: new Date(), deadline: new Date(), end: new Date(), create: new Date(), status: 0, priority: 0 },
]

@Component({
  selector: 'app-details',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatButtonModule,
    MatTableModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    // TODO  убрать лишние провайдеры у остальных DatePickers
    provideMomentDateAdapter(DATE_FORMATS),
  ],
  templateUrl: './details.component.html',
  styleUrl: './details.component.scss'
})
export class DetailsComponent {

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private tasksService: TasksService
  ) { }

  private tasksServiceSubscription = new Subscription;

  minDate = new Date();
  dataSource = ELEMENT_TASK;
  displayedColumns: string[] = ['taskName', 'executors', 'start', 'deadline', 'status', 'priority'];
  statuses = {
    value: ['не начиналось', 'выполняется', 'выполнено'],
    defaultValue: 'не начиналось'
  }
  priorities = {
    value: ['высокий', 'средний', 'низкий'],
    defaultValue: 'средний'
  }

  form: FormGroup<{
    taskName: FormControl<string>,
    executors: FormControl<string>,
    start: FormControl<string>,
    deadline: FormControl<string>,
    status: FormControl<string>,
    priority: FormControl<string>,
  }> = this.formBuilder.group({
    taskName: ['', [Validators.required]],
    executors: ['', [Validators.required]],
    start: ['', [Validators.required]],
    deadline: ['', [Validators.required]],
    status: [this.statuses.defaultValue, [Validators.required]],
    priority: [this.priorities.defaultValue, [Validators.required]],
  });

  onSubmit() {
    // TODO проверка даты, если введена, то должна быть позже текущей
    // Проверить правильное разделение с учетом пробелов

    // console.log(this.form.value.status);
    // console.log(this.form.value.priority);
    // console.log(this.form.value);

    const task: ITask = {
      taskName: this.form.value.taskName!,
      executors: this.form.value.executors!.split(',').join()
        .split('.').join().split(';').join().split(':').join()
        .split(' ').join().split('. ').join().split('; ')
        .join().split('. '),
      start: new Date(this.form.value.start!),
      deadline: new Date(this.form.value.deadline!),
      create: new Date(),
      status: this.statuses.value.indexOf(this.form.value.status!),
      priority: this.priorities.value.indexOf(this.form.value.priority!),
    }

    console.log("this.form.value", task);

    this.tasksServiceSubscription = this.tasksService.create(task).subscribe({
      next: (task) => {
        console.log("создана новая задача:", task);
      },
      error: (error) => {
        console.log("создать новую задачу не удалось", error);
      }
    });


  }

}
