import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { provideNativeDateAdapter } from '@angular/material/core';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';

import { DATE_FORMATS } from '../../models/date-formats';
import { ITask } from '../../models/task';
import { TasksService } from '../../services/tasks.service';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [
    CommonModule,
    NgIf,
    MatTableModule,
    MatSelectModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatButtonModule
  ],
  providers: [
    provideNativeDateAdapter(),
    provideMomentDateAdapter(DATE_FORMATS),
  ],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.scss'
})
export class EditComponent {

  constructor(
    private tasksService: TasksService,
  ) { }

  private tasksServiceSubscription = new Subscription;

  tasks!: ITask[];
  dataSource = new MatTableDataSource(this.tasks);
  selectedElement: number[] = [];
  displayedColumns: string[] = ['taskName', 'executors', 'start', 'deadline', 'status', 'priority'];
  statuses = {
    value: ['не начиналось', 'выполняется', 'выполнено'],
    defaultValue: 'не начиналось'
  }
  priorities = {
    value: ['высокий', 'средний', 'низкий'],
    defaultValue: 'средний'
  }

  onChangeExecutors(names: string | null, element: any) {

    console.log(names);

  }

  ngAfterViewInit() {
    this.tasksServiceSubscription = this.tasksService.fetch().subscribe((items) => {
      this.dataSource.data = items;
    })
  }

  onAddRow() {
    const newRow = { taskName: '', executors: [], start: new Date(), deadline: new Date(), status: 0, priority: 0, }
    // this.dataSource = [...this.dataSource, newRow];
  }

}
