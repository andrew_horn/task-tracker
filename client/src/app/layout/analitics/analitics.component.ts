import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import moment from 'moment';

import { provideNativeDateAdapter } from '@angular/material/core';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
// TODO добавить лоадер
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { provideMomentDateAdapter } from '@angular/material-moment-adapter';

import { ITask } from '../../models/task';
import { IFilter } from '../../models/filter';
import { DATE_FORMATS } from '../../models/date-formats';
import { TasksService } from '../../services/tasks.service';

@Component({
  selector: 'app-analitics',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatCheckboxModule,
    MatSortModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [provideNativeDateAdapter(),
  provideMomentDateAdapter(DATE_FORMATS),
  ],
  templateUrl: './analitics.component.html',
  styleUrl: './analitics.component.scss'
})
export class AnaliticsComponent implements OnInit, OnDestroy {

  @ViewChild('tasksTable') tasksTable = new MatSort();

  constructor(
    private fb: FormBuilder,
    private tasksService: TasksService,
  ) { }

  private tasksServiceSubscription = new Subscription;

  displayedColumns: string[] = ['taskName', 'executors', 'deadline', 'status', 'priority'];
  filters: IFilter[] = [];
  tasks!: ITask[];
  dataSource = new MatTableDataSource(this.tasks);
  load = false;

  datePicker = this.fb.nonNullable.group({
    start: [new Date().toLocaleDateString(), [Validators.required]],
    deadline: [new Date().toLocaleDateString(), [Validators.required]]
  });

  ngOnInit() {
    this.load = true;
    // this.tasksServiceSubscription = this.tasksService.fetch().subscribe((items) => {
    //   this.dataSource.data = items;
    //   this.load = false;
    // })

    this.filters.push({ name: 'executors', options: [''], defaultValue: '' });
    this.filters.push({ name: 'deadline', options: [''], defaultValue: '' });
    this.filters.push({ name: 'status', options: ['All', false, true], defaultValue: 'All' });

    this.dataSource.filterPredicate = function (record, filter) {

      let map = new Map(JSON.parse(filter));
      let isMatch = [true, true, true];

      for (let [key, value] of map) {

        switch (key) {
          case 'executors':
            isMatch[0] = false;
            let coincident: boolean[] = [];
            let valueArr = (`${value}`).split(/[\s,]+/).filter(i => i != "");
            for (let i of valueArr) {
              record.executors.forEach(item => {
                if (item.toLowerCase().includes(i)) coincident.push(true);
              });
            }
            if (coincident.length === valueArr.length) isMatch[0] = true;
            break;
          case 'status':
            isMatch[1] = false;
            isMatch[1] = (value == "All") || (record[key as keyof ITask] == value);
            break;
          case 'deadline':
            isMatch[2] = false;
            let dates = `${value}`.split('*');
            if (record.deadline >= moment(dates[0]).toDate()
              && record.deadline <= moment(dates[1]).toDate()) {
              isMatch[2] = true;
            }
            break;
        }
      }
      for (let i of isMatch) {
        if (!i) return false;
      }
      return true;
    }
  }

  // TODO повтор в ngOnInit, зачем?
  ngAfterViewInit() {
    this.tasksServiceSubscription = this.tasksService.fetch().subscribe((items) => {
      this.dataSource.data = items;
    })
  }

  filterDictionary = new Map();

  applyWorldFilter(event: Event): void {

    this.filterDictionary.set(
      this.filters[0].name,
      (event.target as HTMLInputElement)
        .value.trim().toLocaleLowerCase());

    this.dataSource.filter = JSON.stringify(
      Array.from(this.filterDictionary.entries()));
  }

  applyDateFilter() {
    if (this.datePicker.valid) {
      this.datePicker.disable;
      this.filterDictionary.set(this.filters[1].name,
        `${moment(this.datePicker.value.start).format()}*${moment(this.datePicker.value.deadline).format()}`);
      this.dataSource.filter = JSON.stringify(Array.from(this.filterDictionary.entries()));
      this.datePicker.enable;
    }
  }

  applyDiscretFilter(object: MatSelectChange): void {
    this.filterDictionary.set(this.filters[2].name, object.value);
    this.dataSource.filter = JSON.stringify(
      Array.from(this.filterDictionary.entries()));
  }

  ngOnDestroy(): void {
    this.tasksServiceSubscription.unsubscribe();
  }

}
