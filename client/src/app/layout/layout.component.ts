import { Component, OnDestroy } from '@angular/core';
import { CommonModule, NgFor } from '@angular/common';
import { NavigationEnd, Router, RouterModule, RouterOutlet } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-layout',
  standalone: true,
  imports: [
    RouterModule,
    RouterOutlet,
    CommonModule,
    NgFor,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatButtonToggleModule
  ],
  templateUrl: './layout.component.html',
  styleUrl: './layout.component.scss'
})
export class LayoutComponent implements OnDestroy {

  routerSubscription = new Subscription();
  buttonGroupValue = "/edit";

  constructor(

    private router: Router,

  ) {
    // TODO устанавливаем значение группы кнопок
    // в соответствии со значением текущего маршрута
    this.routerSubscription = this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd && event.url !== '/') {
          this.buttonGroupValue = event.url;
        }
      })
  }

  ngOnDestroy(): void {
    this.routerSubscription.unsubscribe();
  }

}
