import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ITask } from "../models/task";

@Injectable({
    providedIn: 'root'
})
export class TasksService {
    constructor(
        private http: HttpClient
    ) { }

    fetch(): Observable<ITask[]> {
        return this.http.get<ITask[]>('http://localhost:8080/tasks');
        // return this.http.get<ITask[]>('http://tron.ru/api/tasks');
    }

    // getById(id: string): Observable<ITask> {
    //     return this.http.get<ITask>(`/api/category/${id}`);
    // }

    create(task: ITask): Observable<ITask> {

        // const formData = new FormData();

        // if (image) {
        //     formData.append('image', image, image.name);
        //     formData.append('file', image.name);
        // }
        // formData.append('name', name);

        // return this.http.post<ITask>('http://localhost:8080/tasks', task);
        console.log("tasks.service.task", task);

        return this.http.post<ITask>('http://localhost:8080/tasks', task);
    }

    // update(id: string, name: string, image?: File): Observable<ICategory> {

    //     const fd = new FormData();

    //     if (image) {
    //         fd.append('image', image, image.name);
    //     }
    //     fd.append('name', name);

    //     return this.http.patch<ICategory>(`/api/category/${id}`, fd);
    // }

    // delete(id: string): Observable<{ Message: string }> {
    //     return this.http.delete<{ Message: string }>(`/api/category/${id}`);
    // }
}