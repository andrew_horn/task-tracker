import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';


// TODO Сервис пока не пригодился
@Injectable({
    providedIn: 'root'
})
export class MenuStateService {

    routerSubscription = new Subscription();

    constructor(
        private router: Router
    ) {

        this.routerSubscription = this.router.events.subscribe(
            (event) => {
                if (event instanceof NavigationEnd) {
                    if (event.url === '/edit') { }
                    if (event.url === '/analitics') { }
                }
            })
    }
}