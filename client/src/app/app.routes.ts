import { Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { EditComponent } from './layout/edit/edit.component';
import { AnaliticsComponent } from './layout/analitics/analitics.component';
import { DetailsComponent } from './layout/details/details.component';

// TODO Убрать лишнее
export const routes: Routes = [
    {
        path: '', component: LayoutComponent
        , children: [
            { path: '', redirectTo: 'edit', pathMatch: 'full' },
            { path: 'edit', component: EditComponent },
            { path: 'details', component: DetailsComponent },
            { path: 'analitics', component: AnaliticsComponent },
        ]
    }
];
