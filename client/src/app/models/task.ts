export interface ITask {
    taskName: string,
    executors: string[],
    create: Date,
    start: Date,
    deadline: Date,
    end?: Date,
    status: number,
    priority: number,
    // TODO кнопка действия в таблице, когда нужна
    // action: string,
    // TODO если будет выдаваться с бэка
    _id?: string
}
