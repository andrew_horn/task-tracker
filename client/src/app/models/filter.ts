export interface IFilter {
    name: string;
    options: any[];
    defaultValue: string | boolean
}