import { http, HttpResponse } from 'msw'

export const handlers = [
    // Intercept "GET https://example.com/user" requests...
    http.get('http://tron.ru/api/tasks', () => {
        // ...and respond to them using this JSON response.
        console.log('handler: отправлено');
        // setTimeout(() => {
        return HttpResponse.json([

            { task: 'написание проекта', executors: ['Иван'], start: new Date(2024, 3, 16), end: new Date(2024, 3, 25), status: true, priority: 'высокий', action: '' },
            { task: 'тестирование проекта', executors: ['Алексей', 'Иван'], start: new Date(2024, 3, 26), end: new Date(2024, 3, 30), status: true, priority: 'средний', action: '' },
            { task: 'запуск в прод', executors: ['Дмитрий', 'Анатолий', 'Петр'], start: new Date(2024, 4, 1), end: new Date(2024, 4, 15), status: false, priority: 'низкий', action: '' },

        ])
        // }, 1000)
    }
    ),
]